# Del 1
# Sjekker om tallet n er et primtall eller ikke.
# Returnerer True om n er et primtall, False ellers.
def unknown_func(n):
    if n > 1:
        for i in range(2, n//2+1): # '//' er heltallsdivisjon. 6/4=1.5 mens 6//4=1
            if n % i == 0:
                return False
    return True

# Del 2
# Denne koden kjører evig.
# Den vil først printe '###', så '#', og deretter '' (altså ingenting) evig.
# For at while-løkken ikke skulle kjørt evig, så måtte x ha blitt 0 en gang.
# 0 er det eneste tallet som blir sett på som False. Alle andre telles som True.
x = 3
while x:
    print("#"*x)
    x -= 2

# Del 3
# Denne funksjonen regner ut summen av alle sifferene i argumentet a som blir tatt inn.
# a = 1234 gir r = 10
def unknown_func2(a):
    r = 0
    while a > 0:
        s = a % 10
        r = r + s
        a = (a - s)//10  # Bruker heltallsdivisjon her
    return r

print(unknown_func2(1234))
