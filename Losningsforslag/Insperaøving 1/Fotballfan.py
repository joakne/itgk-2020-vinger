result = input("Hva ble resultatet? ").lower()

if result == 'h':
    print("Yay, det ble hjemmeseier!")
elif result == 'b':
    print("Oufh, så trist å tape på egen bane.")
elif result == 'u':
    print("Snufs, men kunne vært verre.")
else:
    print('Gikk det såpass dårlig ja? Ønsker ikke å snakke om det engang...')