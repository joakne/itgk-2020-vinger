# Del 1
def triangle(h):
    for i in range(1,h+1):
        print("* "*i)

triangle(5)

# Del 2
def flipped_triangle(h):
    for i in range(h,0,-1):
        print("* " * i)

flipped_triangle(5)

# Del 3
def isosceles_triangle(h):
    count = h-1
    for i in range(1,h+1):
        space = " "*count
        str = "* " * i
        print(space, str)
        count = count - 1

isosceles_triangle(5)
